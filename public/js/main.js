window.onload = function() {
  Particles.init({ options });
};

// e.g.
window.onload = function() {
  Particles.init({
    selector: '#particles',
    color: '#5cdced',
    speed: '0.2',
    sizeVariations: '3',
    maxParticles: '100',
    minDistance: '150',
    connectParticles: 'true'
  });
};